from cmath import pi, exp
from itertools import count
from math import e
import copy
import math


operation_list = ['+','-','*','/']

def sqrt(oper):
    return(math.sqrt(oper))    

def sin(oper):
    return math.sin(oper)

def asin(oper):
    return math.asin(oper)

def cos(oper):
    return math.cos(oper)

def acos(oper):
    return math.acos(oper)

def tan(oper):
    return math.tan(oper)

def atan(oper):
    return math.atan(oper)

def cotan(oper):
    return 1/math.tan(oper)

def atanh(oper):
    return math.atanh(oper)

def log(oper1, oper2):
    return math.log(oper1, oper2)


def Precedence_Order(opd):
    
    if opd == '+' or opd == '-':
        return 1
    if opd == '*' or opd == '/':
        return 2
    return 0
 
def Operation(num1, num2, opd):
     
    if opd == '+': return num1 + num2
    if opd == '-': return num1 - num2
    if opd == '*': return num1 * num2
    if opd == '/': return num2 / num1

 
def Evaluate(expression):
     
    values = []
    ops = []
    i = 0

    while i < len(expression):
         
        if expression[i] == ' ':
            i += 1
            continue
        
        elif expression[i:i+2] == 'pi':
            values.append(pi)
            i += 1
        
        elif expression[i] == 'e':
            values.append(e)
            i += 1
        
        elif expression[i].isalpha():
            string = ""
            while expression[i].isalpha():
                string += expression[i]
                i += 1
            i -= 1
            ops.append(string)

        elif expression[i] == '(':
            ops.append(expression[i])

        elif expression[i].isdigit():
            val = 0
             
            while (i < len(expression) and expression[i].isdigit()):
                val = (val * 10) + int(expression[i])
                i += 1
             
            values.append(val)
             
            i-=1
        
        elif expression[i] == ')':
         
            while len(ops) != 0 and ops[-1] != '(':
                val1 = values.pop()
                opd = ops.pop()
                if opd not in operation_list:
                    result = globals()[opd](val1)
                else:
                    val2 = values.pop()
                    result = Operation(val1, val2, opd)  
                values.append(result)
            ops.pop()
            
         
        else:
            while (len(ops) != 0 and Precedence_Order(ops[-1]) >= Precedence_Order(expression[i])):
                opd = ops.pop()
                val1 = values.pop()
                val2 = values.pop()
                values.append(Operation(val1, val2, opd))
             
            
            ops.append(expression[i])
         
        i += 1

    ops_new = copy.deepcopy(ops)
    count = 0
    for i, opd in enumerate(ops):
        if opd not in operation_list:
            values[i-count] = globals()[opd](values[i-count]) 
            ops_new.remove(opd)
            count += 1

    while len(ops_new) != 0:
         
        val1 = values.pop()
        opd = ops_new.pop()
        # print(i, values, ops)
        if opd not in operation_list:
            result = globals()[opd](val1)
        else:
            val2 = values.pop()
            result = Operation(val1, val2, opd)
        
        values.append(result)
     
    return values[-1]

while True:
    print("Type q to exit")
    expression = input()
    if expression == "q":
        break
    print(Evaluate(expression))